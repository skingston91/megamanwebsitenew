import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';

import { ContentDetail } from 'components/ContentDetail';
import { Header } from 'components/Header';
import { ContentList } from 'components/ContentList';
import { Footer } from 'components/Footer';
import { FooterDecoration } from 'components/FooterDecoration';

import content from 'data/content';

//megaSteve images
import upLeft from 'assets/images/megasteve/upLeft.png';
import up from 'assets/images/megasteve/up.png';
import upRight from 'assets/images/megasteve/upRight.png';
import left from 'assets/images/megasteve/left.png';
import center from 'assets/images/megasteve/center.png';
import right from 'assets/images/megasteve/right.png';
import downLeft from 'assets/images/megasteve/downLeft.png';
import down from 'assets/images/megasteve/down.png';
import downRight from 'assets/images/megasteve/downRight.png';

const megamanImages = [upLeft, up, upRight, left, center, right, downLeft, down, downRight];

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeSelection: undefined
    };
  }

  contentHover(id) {
    const position = id.activeHoverId;
    const image = megamanImages[position];
    let item = content[4];
    item.thumbnail = image;
    content[4] = item;

    this.setState({ selectedContent: content });
  }

  render() {
    const { activeSelection } = this.state;
    const currentDate = new Date();
    return (
      <div className="App">
        <DocumentTitle title="KingstonDev" />
        <Header text="Kingston Development" />
        {activeSelection && (
          <ContentDetail
            content={activeSelection}
            onContentSelect={activeSelection => this.setState({ activeSelection })}
          />
        )}
        {content && (
          <ContentList
            content={content}
            onContentSelect={activeSelection => this.setState({ activeSelection })}
            onContentHover={activeHoverId => this.contentHover({ activeHoverId })}
          />
        )}
        <FooterDecoration />
        <Footer text={`©${currentDate.getFullYear()} KingstonDev. All rights reserved.`} />
      </div>
    );
  }
}
