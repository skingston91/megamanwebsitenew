import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

export const Header = ({ text }) => (
  <div className="header fancy megaText">
    <span>{text}</span>
  </div>
);

Header.propTypes = {
  text: PropTypes.string.isRequired
};
