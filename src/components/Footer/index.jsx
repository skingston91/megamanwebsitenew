import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

export const Footer = ({ text }) => {
  return <div className="footer megaText">{text}</div>;
};

Footer.propTypes = {
  text: PropTypes.string.isRequired
};
