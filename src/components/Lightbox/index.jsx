import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

export const Lightbox = ({ handleClick, children, ...rest }) => (
  <div className="Lightbox" onClick={handleClick} {...rest}>
    {children}
  </div>
);

Lightbox.propTypes = {
  handleClick: PropTypes.func.isRequired
};
