import React from 'react';
import PropTypes from 'prop-types';

import { ContentListItem } from 'components/ContentListItem';

import './styles.scss';

export const ContentList = ({ content, onContentSelect, onContentHover }) => {
  return (
    <div className="ContentList">
      {content
        .map((item, index) => (
          <ContentListItem
            key={item.title}
            onContentSelect={onContentSelect}
            onContentHover={onContentHover}
            position={index}
            content={item}
          />
        ))
        .reduce(
          (groups, item) => {
            const lastGroup = groups[groups.length - 1];
            if (lastGroup.length < 3) {
              lastGroup.push(item);
            } else {
              groups.push([item]);
            }
            return groups;
          },
          [[]]
        )
        .map((group, groupId) => (
          <div className="list-row-container" key={groupId}>
            <div className="list-row">{group}</div>
          </div>
        ))}
    </div>
  );
};

ContentList.propTypes = {
  onContentSelect: PropTypes.func.isRequired,
  onContentHover: PropTypes.func.isRequired,
  content: PropTypes.array.isRequired
};
