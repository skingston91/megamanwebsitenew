import React from 'react';
import PropTypes from 'prop-types';

import { Lightbox } from 'components/Lightbox';

import './styles.scss';

export const ContentDetail = ({ onContentSelect, content }) => (
  <Lightbox handleClick={() => onContentSelect(null)}>
    <div className="content" onClick={() => (window.location.href = content.url)}>
      <span className="content-extra-corners" />
      <div className="content-container">
        <img className="content-img" {...content.image} />
        <div className="content-detail megaText">
          <div className="content-title">{content.title}</div>
          <div className="content-description">{content.description}</div>
          <div className="content-guide">
            {content.url === ''
              ? 'Please wait to hear more information on this'
              : 'Click in the box to see more'}
          </div>
        </div>
      </div>
    </div>
  </Lightbox>
);

ContentDetail.propTypes = {
  onContentSelect: PropTypes.func.isRequired,
  content: PropTypes.object.isRequired
};
