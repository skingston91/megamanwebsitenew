import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

const splitText = text => {
  const textArray = text.split(' ');
  return textArray.map((text, index) => (
    <div key={index} className="list-item-title megaText">
      <span>{text}</span>
    </div>
  ));
};

export const ContentListItem = ({ content, onContentSelect, position, onContentHover }) => (
  <div className="list-item list-group-item:hover">
    <div className="list-item-container" onMouseEnter={() => onContentHover(position)}>
      <span className="list-item-extra-corners" />
      <div className="list-item-image-container" onClick={() => onContentSelect(content)}>
        <img className="list-item-img" {...content.thumbnail} />
      </div>
    </div>
    <div className="list-item-detail">{splitText(content.title)}</div>
  </div>
);

ContentListItem.propTypes = {
  onContentSelect: PropTypes.func.isRequired,
  onContentHover: PropTypes.func.isRequired,
  content: PropTypes.object.isRequired,
  position: PropTypes.number.isRequired
};
