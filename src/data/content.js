import protest5 from 'assets/images/Protest-5.jpg';
import protestOriginal from 'assets/images/Protest.png';
import indieBooks from 'assets/images/indieBooks.jpg';
import race from 'assets/images/Race.png';
import projectIcarus from 'assets/images/ProjectIcarus.png';
import megaSteve from 'assets/images/megasteve/center.png';
import me from 'assets/images/Me.jpg';
import careersappSmall from 'assets/images/careers-app-small.png';
import careersapp from 'assets/images/Careers-App.png';
import cyberSpace from 'assets/images/CyberSpace.png';
import comingSoon from 'assets/images/Coming-Soon.png';
import storybook from 'assets/images/Storybook.png';
import blender from 'assets/images/Blender.png';

export const content = [
  {
    title: 'Protest Game',
    thumbnail: protest5,
    image: protestOriginal,
    description:
      "Created in Unity4 for Edge's get into games competition. This features a fully functional dynamic story driven game with multiple endings. This was created with a group of friends in our spare time in 2 months.",
    url: 'http://app.kingstondev.co.uk/Protest-Jam.html'
  },
  {
    title: 'Indie Books',
    thumbnail: indieBooks,
    image: indieBooks,
    description:
      'Created for an university E-Commerce assessment, the site features ssl encryption, downloadable text files, secure-logins, integration with google checkout and secure audit logs and trail.',
    url: 'http://app.kingstondev.co.uk/indieBooks/index.php'
  },
  {
    title: 'Canvas Game',
    thumbnail: race,
    image: race,
    description:
      'Created in a weekend to learn canvas, this contains a name generator, and a fully functional betting system',
    url: 'http://app.kingstondev.co.uk/betting-game/Game.html'
  },
  {
    title: 'Project Icarus',
    thumbnail: projectIcarus,
    image: projectIcarus,
    description:
      "The long term project, working as a part of a small team building a prototype of a fully 3D cinematic role playing game including an active battle system and exploration. In it's current form, it contains a working battle system, dynamic exploration,functional menus, music and sound.",
    url: 'https://www.youtube.com/watch?v=BMAfZBcVpCg'
  },
  {
    title: 'Steven Kingston',
    thumbnail: megaSteve,
    image: megaSteve,
    description:
      'Welcome to my website, I program, travel, game and procrastinate? This is my place to store past and present projects. I hope you enjoy a glimpse of my work! :)',
    url: 'https://twitter.com/StevenKingston1'
  },
  {
    title: 'Careers App',
    thumbnail: careersappSmall,
    image: careersapp,
    description:
      'Created for a school in Canterbury as part of final year two man group project. This followed a full project life cycle from interviews with teachers and students to integration into the schools eco-system. This was a nine month project.',
    url: 'http://app.kingstondev.co.uk/careersapp/loginPage.php'
  },
  {
    title: 'Cyber Space',
    thumbnail: cyberSpace,
    image: cyberSpace,
    description:
      'Long term development project to render 2D webpage’s content into 3D web spaces. Currently rendering different 3D elements based off the webpage’s content. Current build allows movement between different webpages and websites whilst allowing people to other people active on the server.',
    url: 'https://youtu.be/XT9u1ZvMD9c'
  },
  {
    title: 'Component Library',
    thumbnail: storybook,
    image: storybook,
    description:
      'This is a frontend component library, that I created as part of a previous role. This is an early build, taken (with permission) at the time I left the company. The components are built in TypeScript with React, and the styling is using LESS with BEM.',
    url: 'https://kingstondevstorybook.now.sh'
  },
  {
    title: 'Blender Work',
    thumbnail: blender,
    image: blender,
    description:
      'Over the course of university and in my spare time since I have been building some basic Blender models. Linked to this page is a Youtube video of animation for one of my university assessments. The animation is done in a different rendering process to save render time :)',
    url: 'https://www.youtube.com/watch?v=pzRsLibGJjA'
  }
];

export default content;
